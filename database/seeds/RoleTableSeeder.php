<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new \App\Granted\Role();
        $owner->name         = 'manager';
        $owner->display_name = 'Project Manger';
        $owner->description  = 'User is the manager of a given project';
        $owner->save();
    }
}
