<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                'name' => 'Nike 2017 Shoes',
                'description' => 'The best seller shoes of the year',
                'price' => 200,
                'imgName' => "nike1",
                'imgExt' => "jpg",
            ]
        );

        DB::table('products')->insert(
            [
                'name' => 'Nike 2014 Shoes',
                'description' => 'The best seller shoes of the year',
                'price' => 185,
                'imgName' => "nike2",
                'imgExt' => "jpg",
            ]
        );

        DB::table('products')->insert(
            [
                'name' => 'Nike 2015 Shoes',
                'description' => 'The best seller shoes of the year',
                'price' => 150,
                'imgName' => "nike3",
                'imgExt' => "jpg",
            ]
        );
    }
}
