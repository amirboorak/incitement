<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'john',
                'email' => 'john@example.com',
                'password' => bcrypt('123456'),
            ]
        );

        DB::table('users')->insert(
            [
                'name' => 'jane',
                'email' => 'jane@example.com',
                'password' => bcrypt('123456'),
            ]
        );

        DB::table('users')->insert(
            [
                'name' => 'kenny',
                'email' => 'kenny@example.com',
                'password' => bcrypt('123456'),
            ]
        );
    }
}
