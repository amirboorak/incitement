<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html"
             data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li>
                        <a href="{{ route('dashboard') }}">
                            <i class="fa fa-tachometer" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                        <a href="{{ route('new_product') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <span>New Product</span>
                        </a>
                        <a href="{{ route('list_product') }}">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            <span>Product List</span>
                        </a>
                        <a href="{{ route('attach_role') }}">
                            <i class="fa fa-paperclip" aria-hidden="true"></i>
                            <span>Attach User Role</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</aside>
