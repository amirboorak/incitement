@extends('base')

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                    </div>
                    <h2 class="panel-title">New Product</h2>
                </header>
                <div class="panel-body">
                    {!! Form::open(['action' => 'ProductController@store','class' => 'form-horizontal form-bordered','enctype' => 'multipart/form-data']) !!}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Name</label>
                        <div class="col-md-6">
                            {!! Form::text('name','',array_merge(['class' => 'form-control'])) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDisabled">Price</label>
                        <div class="col-md-6">
                            {!! Form::text('price','',array_merge(['class' => 'form-control'])) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputHelpText">Description</label>
                        <div class="col-md-6">
                            {!! Form::textarea('description','',array_merge(['class' => 'form-control'])) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tags-input" class="col-md-3 control-label">Tags</label>
                        <div class="col-md-6">
                            {!! Form::text('tags','',array_merge(['class' => 'form-control','data-role' => 'tagsinput','data-tag-class' => 'label label-primary','data-role' => 'tagsinput','id' => 'tags-input'])) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tags-input" class="col-md-3 control-label">Upload Image</label>
                        <div class="col-md-6">
                            {!! Form::file('image') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputHelpText"></label>
                        <div class="col-md-6">
                            {!! Form::submit('Add',['class' => 'btn btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection

