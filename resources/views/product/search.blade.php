@extends('base')

@section('body')
    <section class="content-with-menu content-with-menu-has-toolbar media-gallery">
        <div class="content-with-menu-container">
            <div class="inner-body mg-main">
                <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
                                <div class="thumbnail">
                                    <div class="thumb-preview">
                                        <a class="thumb-image" href="#">
                                            <img src="{{asset('images/'.$product->imgName . "." . $product->imgExt)}}"
                                                 class="img-responsive" alt="Project">
                                        </a>
                                    </div>
                                    <h5 class="mg-title text-weight-semibold">{{$product->name}}</h5>
                                    <div class="mg-description">
                                        <small class="pull-left text-muted">$ {{$product->price}}</small>
                                    </div>
                                    <hr class="dotted short">
                                    <div class="mg-description">
                                        <small class="pull-left text-muted">{{$product->description}}</small>
                                    </div>
                                    <hr class="dotted short">
                                    <div class="mg-description">
                                        @foreach($product->tags as $tag)
                                            <span class="label label-success">{{$tag->tag}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-success">
                            <strong>No Result!</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection