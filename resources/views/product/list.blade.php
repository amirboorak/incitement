@extends('base')

@section('body')
    @role('manager')
    <div class="search-control-wrapper">
        {!! Form::open(['action' => 'ProductController@search', 'method' => 'get']) !!}
        <div class="form-group">
            <div class="input-group">
                {!! Form::text('keyword','',array_merge(['class' => 'form-control','placeholder' => 'Search By Tag'])) !!}
                <span class="input-group-btn">
                         {!! Form::submit('Search',['class' => 'btn btn-primary']) !!}
                </span>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @endrole
    <section class="content-with-menu content-with-menu-has-toolbar media-gallery">
        <div class="content-with-menu-container">
            <div class="inner-body mg-main">
                <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                    @foreach($products as $product)
                        <div class="isotope-item image col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="#">
                                        <img src="{{asset('images/'.$product->imgName . "." . $product->imgExt)}}"
                                             class="img-responsive" alt="Project">
                                    </a>
                                </div>
                                <h5 class="mg-title text-weight-semibold">{{$product->name}}</h5>
                                <div class="mg-description">
                                    <small class="pull-left text-muted">$ {{$product->price}}</small>
                                </div>
                                <hr class="dotted short">
                                <div class="mg-description">
                                    <small class="pull-left text-muted">{{$product->description}}</small>
                                </div>
                                <hr class="dotted short">
                                <div class="mg-description">
                                    @foreach($product->tags as $tag)
                                        <span class="label label-success">{{$tag->tag}}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection