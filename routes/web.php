<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/product/new', 'ProductController@new')->name('new_product');
    Route::post('/dashboard/product/store', 'ProductController@store')->name('store_product');
    Route::get('/home', 'HomeController@home')->name('home');
    Route::get('/dashboard/product/list', 'ProductController@list')->name('list_product');
    Route::get('/dashboard/product/search', 'ProductController@search');
    Route::get('/dashboard/role', 'HomeController@attachUserRole')->name('attach_role');
});
