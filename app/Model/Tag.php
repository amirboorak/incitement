<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App\Model
 *
 * @author Amir Boorak <sa.shojaei92@gmail.com>
 */
class Tag extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * Many to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Model\Post');
    }
}
