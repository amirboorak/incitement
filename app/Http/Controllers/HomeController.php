<?php

namespace App\Http\Controllers;

use App\Granted\Role;
use App\User;

/**
 * Class HomeController
 * @package App\Http\Controllers
 *
 * @author Amir Boorak <sa.shojaei92@gmail.com>
 */
class HomeController extends Controller
{

    /**
     * @return string
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('home');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function attachUserRole()
    {
        $user = User::where('name', '=', 'kenny')->first();
        if (count($user->roles()->get()) == 0) {
            $roleId = Role::where('name', 'manager')->first();
            $user->roles()->attach($roleId);
            flash('Role added Successfully');
        } else {
            flash('Role Has been attached!!!');
        }
        return redirect('/dashboard');


    }
}
