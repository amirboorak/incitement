<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Repository\ProductRepository;
use App\Repository\TagRepository;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers
 *
 * @author Amir Boorak <sa.shojaei92@gmail.com>
 */
class ProductController extends Controller
{

    /**
     * @var ProductRepository
     */
    protected $product_repo;

    /**
     * @var TagRepository
     */
    protected $tag_repo;

    /**
     * ProductController constructor.
     * @param ProductRepository $product
     * @param TagRepository $tag
     */
    public function __construct(ProductRepository $product, TagRepository $tag)
    {
        $this->product_repo = $product;
        $this->tag_repo = $tag;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        return view('product.new');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|integer',
            'description' => 'required',
            'tags' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png,jpg',
        ]);

        if ($request->isMethod('POST')) {
            $this->product_repo->store($request->all());
            flash('Product added Successfully');
            return redirect('dashboard/product/new');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $products = Product::all();
        return view('product.list', ["products" => $products]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required',
        ]);

        $keyword = $request->input('keyword');
        $id = $this->tag_repo->getTagIdByKeyword($keyword);
        $products = $this->product_repo->searchResult($id);
        return view('product.search', ["products" => $products]);
    }

}
