<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\Tag;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 *
 * @author Amir Boorak <sa.shojaei92@gmail.com>
 */
class DashboardController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.index', ["products" => $this->getProudctsCount(), "tags" => $this->getTagsCount()]);
    }

    /**
     * @return int
     */
    private function getProudctsCount()
    {
        return count(Product::all());
    }

    /**
     * @return int
     */
    private function getTagsCount()
    {
        return count(Tag::all());
    }
}
