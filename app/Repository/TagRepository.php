<?php


namespace App\Repository;

use App\Model\Tag;

/**
 * Class TagRepository
 *
 * @author Amir Boorak <sa.shojaei92@gmail.com>
 */
class TagRepository extends BaseRepository
{
    /**
     * @var Tag
     */
    private $tag_model;

    /**
     * TagRepository constructor.
     * @param Tag $tag
     */
    public function __construct(Tag $tag)
    {
        $this->tag_model = $tag;
    }

    /**
     * @param $keyword
     * @return mixed
     */
    public function getTagIdByKeyword($keyword)
    {
        if ($this->getTag($keyword)) {
            return $this->tag_model->select()->where('tag', $keyword)->first()->id;
        } else {
            return;
        }
    }

    /**
     * @param $keyword
     * @return bool
     */
    private function getTag($keyword)
    {
        $tag = $this->tag_model->select()->where('tag', $keyword)->first();
        if ($tag === null) {
            return false;
        } else {
            return true;
        }
    }

}