<?php

namespace App\Repository;

use App\Model\Product;
use App\Model\Tag;
use Illuminate\Support\Facades\Input;

/**
 * Class ProductRepository
 * @package App\Repository
 *
 * @author Amir Boorak <sa.shojaei92@gmail.com>
 */
class ProductRepository extends BaseRepository
{
    /**
     * @var Product
     */
    protected $product_model;

    /**
     * @var Tag
     */
    protected $tag_model;

    /**
     * ProductRepository constructor.
     * @param Product $product
     * @param Tag $tag
     */
    public function __construct(Product $product, Tag $tag)
    {
        $this->product_model = $product;
        $this->tag_model = $tag;
    }

    /**
     * @param $data
     */
    public function store($data)
    {
        $name = uniqid();
        $file = Input::file('image');
        $product = $this->saveData($data, $name, $file);
        $this->saveTags($data, $product);
        $this->saveImage($file, $this->getExt($file), $name);
    }


    /**
     * @param $data
     * @param $imgName
     * @param $file
     * @return Product
     */
    private function saveData($data, $imgName, $file)
    {
        $product = new Product();
        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->price = $data['price'];
        $product->imgName = $imgName;
        $product->imgExt = $this->getExt($file);
        $product->save();
        return $product;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function searchResult($id)
    {
        $product = Product::with('tags');
        $query = $product->whereHas('tags', function ($q) use ($id) {
            $q->where('tags.id', $id);
        });
        return $query->get();
    }

    /**
     * @param $data
     * @param $product
     */
    private function saveTags($data, $product)
    {
        if (array_key_exists('tags', $data) && $data['tags'] != '') {
            $tags = explode(',', $data['tags']);
            foreach ($tags as $tag) {
                $tagRef = $this->tag_model->whereTag($tag)->first();
                if (is_null($tagRef)) {
                    $tagRef = new $this->tag_model;
                    $tagRef->tag = $tag;
                    $product->tags()->save($tagRef);
                } else {
                    $product->tags()->attach($tagRef->id);
                }
            }
        }
    }

    /**
     * @param $file
     * @param $ext
     * @param $name
     */
    private function saveImage($file, $ext, $name)
    {
        $file->move('images', $name . "." . $ext);
    }

    /**
     * @param $file
     * @return mixed
     */
    private function getExt($file)
    {
        $arr = explode(".", $file->getClientOriginalName());
        return end($arr);
    }

}