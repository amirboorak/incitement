<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ProductTest extends TestCase
{
    public function testNewProductForm()
    {
        $data = [
            "name" => "Nike",
            "description" => "This is a test product",
            "price" => 12345,
            "tags" => $this->generateRandomString(),
            'image' => UploadedFile::fake()->image('test.png'),
        ];
        $response = $this->post('/dashboard/product/store', $data);
        $response->assertRedirect('/dashboard/product/new');
    }

    /**
     * @param int $length
     * @return string
     */
    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
