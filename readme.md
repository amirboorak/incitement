Laravel 5.4 <sa.shojaei92@gmail.com>

Installation

git clone https://amirboorak@bitbucket.org/amirboorak/incitement.git

cd projectname

composer install

Create a database in .env

Change CACHE_DRIVER=file to CACHE_DRIVER=array in .env file

php artison key:generate

php artison migrate --seed

*Setup Role hierarchy configuration

1.Login to the dashboard 
    
    - This are the passwords:
        
        email: john@example.com
        password: 123456
        
        email: jane@example.com
        password: 123456
        
        email: kenny@example.com
        password: 123456
        role: manager
        
2.Click On Attach User Role Button

Now you can add product with tags and you will see the search box top of the page. more over you can search products by tags (only for manager role)